.test:
  variables:
    DEBIAN_FRONTEND: noninteractive
    XDEBUG_MODE: coverage
    TEST_SERVER_PORT: 8080
    MYSQL_HOSTNAME: mysql
    MYSQL_ROOT_PASSWORD: root_password
    MYSQL_DATABASE: test
    MYSQL_USER: test
    MYSQL_PASSWORD: password
    POSTGRES_HOSTNAME: postgres
    POSTGRES_HOST_AUTH_METHOD: trust
    POSTGRES_DB: test
    POSTGRES_USER: test
    POSTGRES_PASSWORD: password

  services:
  - mysql:latest
  - postgres:latest

  before_script:
  - apt-get update

  # install dependency for composer to run
  - apt-get -yq install git unzip zip libzip-dev zlib1g-dev

  # install dependency for firefox
  - |
    # install dependency for firefox
    apt-get -yq install \
      libdbus-glib-1-2 \
      libgd-dev \
      libgtk-3-0 \
      libicu-dev \
      libpng-dev \
      libx11-6 \
      libx11-xcb1

  # install dependency for php plugins
  - |
    # install dependency for php plugins
    apt-get -yq install \
       libsqlite3-dev \
       libpq-dev

  # install and enable xdebug
  - pecl install xdebug
  - docker-php-ext-install zip pdo_mysql pdo_sqlite pdo_pgsql
  - docker-php-ext-enable xdebug pdo_mysql pdo_sqlite pdo_pgsql

  # properly setup php
  - cp -pdf /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini

  # install composer
  - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  - php composer-setup.php --install-dir=/usr/local/bin --filename=composer
  - php --ini

  # install firefox and put a symlink to /usr/local/bin
  - |
    # install firefox
    curl -sL 'https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US' | tar xj --directory=/usr/local
    ln -s /usr/local/firefox/firefox /usr/local/bin/firefox

  # create test user to test because require
  # to test case with unreadable / unwritable folder.
  - useradd testuser --create-home
  - chown testuser -R .

  # install library dependencies for composer
  - su testuser -c 'composer install'

  # install webdrivers for acceptance tests
  - su testuser -c 'composer test:download:monogatari'
  - su testuser -c 'composer test:download:webdriver'

  script:
  - su testuser -c "./tests/_utils/_generated/geckodriver >tests/_output/geckodriver.log 2>&1 &"
  - su testuser -c "php -S 127.0.0.1:${TEST_SERVER_PORT} -t ./examples >tests/_output/example-server.log 2>&1 &"
  - su testuser -c 'composer test:codecept:unit -- --coverage-xml --coverage-html --coverage-text --coverage-cobertura=cobertura.xml'
  - su testuser -c 'composer test:phpstan -- --no-progress'
  - su testuser -c 'composer test:codecept:acceptance'
  - |
    # make sure the coverage analyser has something to analyse at the end (without ANSI color).
    echo "Test Coverage:"
    grep --color=never '^  Lines:' tests/_output/coverage.txt

  artifacts:
    when: always
    paths:
    - example/gameSave.json
    - tests/_output
    reports:
      cobertura: tests/_output/cobertura.xml
    expire_in: 1 month

test:8.0:
  image: php:8.0
  extends:
  - .test

test:8.1:
  image: php:8.1
  extends:
  - .test

packagist:
  stage: deploy
  image: php:8.1
  script:
  - echo "branch=$CI_DEFAULT_BRANCH commit=$CI_COMMIT_REF_NAME tag=$CI_COMMIT_TAG"
  - >
    curl -s -XPOST \
      -H'content-type:application/json' \
      "https://packagist.org/api/update-package?username=$PACKAGIST_USER&apiToken=$PACKAGIST_TOKEN" \
      -d'{"repository":{"url":"https://packagist.org/packages/monogatari-php/remote-storage"}}' | \
      php -r '$s = fopen("php://stdin", "r"); $j = json_decode(stream_get_contents($s)); if ($j->status === "success") exit("success"); echo $s; exit(1);'
  rules:
  - if: '($PACKAGIST_USER != "" && $PACKAGIST_TOKEN != "") && ($CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME || $CI_COMMIT_TAG != null)'

include:
- template: Security/Dependency-Scanning.gitlab-ci.yml
