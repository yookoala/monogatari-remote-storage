<?php

namespace Monogatari\RemoteStorage\StorageEngine\DatabaseStorage;

use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;
use Monogatari\RemoteStorage\StorageEngine\StorageInterface;

abstract class PDOStorage implements StorageInterface
{

    /**
     * The database connection.
     *
     * @var \PDO
     */
    protected $pdo;

    /**
     * The table name to use.
     *
     * @var string
     */
    private $tableName;

    /**
     * The store name for the storage.
     *
     * @var string
     */
    private $storeName;

    /**
     * The user identifier to use.
     *
     * @var string
     */
    private $userId;

    /**
     * Class constructor.
     *
     * @param \PDO        $pdo        Database connection.
     * @param string|null $tableName  Table name.
     * @param string|null $storeName  Store name of the storage.
     * @param string|null $userId     User identifier.
     */
    public function __construct(\PDO $pdo, ?string $tableName = null, ?string $storeName = null, ?string $userId = null)
    {
        $this->pdo = $pdo;
        $this->tableName = $tableName ?: 'monogatari_storage';
        $this->storeName = $storeName ?? '';
        $this->userId = $userId ?? '';
        $this->init();
    }

    /**
     * Initialize the database and table for storage.
     *
     * @return boolean
     *
     * @throws \PDOException
     */
    abstract protected function init(): bool;

    /**
     * Prepare a PDOStatement for getAll operation.
     *
     * Must accept argument :user_id and :store_name.
     *
     * @return \PDOStatement
     */
    abstract protected function prepareGetAllStatement(): \PDOStatement;

    /**
     * Prepare a PDOStatement for get operation.
     *
     * Must accept argument :user_id, :store_name and :key.
     *
     * @return \PDOStatement
     */
    abstract protected function prepareGetStatement(): \PDOStatement;

    /**
     * Prepare a PDOStatement for set operation.
     *
     * Must accept argument :user_id, :store_name, :key and :value.
     *
     * @return \PDOStatement
     */
    abstract protected function prepareSetStatement(): \PDOStatement;

    /**
     * Prepare a PDOStatement for remove operation.
     *
     * Must accept argument :user_id, :store_name and :key.
     *
     * @return \PDOStatement
     */
    abstract protected function prepareRemoveStatement(): \PDOStatement;

    /**
     * Prepare a PDOStatement for clear operation.
     *
     * Must accept argument :user_id and :store_name.
     *
     * @return \PDOStatement
     */
    abstract protected function prepareClearStatement(): \PDOStatement;

    /**
     * Get the PDO object from the constructor.
     *
     * @return \PDO
     */
    protected function getPDO(): \PDO
    {
        return $this->pdo;
    }

    /**
     * Get the table name for the storage.
     *
     * @return string
     */
    protected function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Get the store name for the storage.
     *
     * @return string
     */
    protected function getStoreName(): string
    {
        return $this->storeName;
    }

    /**
     * Get the user identifier for the storage.
     *
     * @return string
     */
    protected function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * Converts the string "{tableName}" into actual table name provided.
     *
     * @param string $sql
     * @param string $tableName
     *
     * @return string
     */
    protected static function useTablename(string $sql, string $tableName): string
    {
        return strtr($sql, ['{tableName}' => $tableName]);
    }

    /**
     * {@inheritDoc}
     *
     * @throws \PDOException
     */
    public function getAll(): object
    {
        $stmt = $this->prepareGetAllStatement();
        $stmt->execute([
            ':user_id' => $this->getUserId(),
            ':store_name' => $this->getStoreName(),
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
        if ($result === false) {
            // Note: \PDO::FETCH_KEY_PAIR usually doesn't return false.
            //       This is simply a precausion.
            return (object) [];
        }
        return (object) $result;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \PDOException
     */
    public function get(string $key)
    {
        $stmt = $this->prepareGetStatement();
        $stmt->execute([
            ':user_id' => $this->getUserId(),
            ':store_name' => $this->getStoreName(),
            ':key' => $key,
        ]);
        $valueRaw = $stmt->fetchColumn();
        if ($valueRaw === false) {
            throw new StorageKeyNotFound($key);
        }
        return json_decode($valueRaw);
    }

    /**
     * {@inheritDoc}
     *
     * @throws \PDOException
     */
    public function set(string $key, $value)
    {
        $this->prepareSetStatement()->execute([
            ':user_id' => $this->getUserId(),
            ':store_name' => $this->getStoreName(),
            ':key' => $key,
            ':value' => json_encode($value),
        ]);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function remove(string $key)
    {
        $this->prepareRemoveStatement()->execute([
            ':user_id' => $this->getUserId(),
            ':store_name' => $this->getStoreName(),
            ':key' => $key,
        ]);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function clear(): bool
    {
        return $this->prepareClearStatement()->execute([
            ':user_id' => $this->getUserId(),
            ':store_name' => $this->getStoreName(),
        ]);
    }
}
