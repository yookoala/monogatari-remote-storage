<?php

namespace Monogatari\RemoteStorage\StorageEngine\DatabaseStorage;

use Monogatari\RemoteStorage\StorageEngine\DatabaseStorage\PDOStorage;

class PgSqlStorage extends PDOStorage
{

    /**
     * {@inheritDoc}
     */
    protected function init(): bool
    {
        $stmt = $this->getPDO()->prepare(static::useTablename('
            CREATE TABLE IF NOT EXISTS "{tableName}" (
                "user_id" VARCHAR(255) NOT NULL DEFAULT \'\',
                "store_name" VARCHAR(255) NOT NULL DEFAULT \'\',
                "key" VARCHAR(255) NOT NULL DEFAULT \'\',
                "value" JSON,
                PRIMARY KEY ("user_id", "store_name", "key")
            );
        ', $this->getTableName()));
        return $stmt->execute();
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareGetAllStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            SELECT "key", "value" FROM "{tableName}" WHERE
                "user_id" = :user_id AND
                "store_name" = :store_name;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareGetStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            SELECT "value" FROM "{tableName}" WHERE
                "user_id" = :user_id AND
                "store_name" = :store_name AND
                "key" = :key;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareSetStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            INSERT INTO "{tableName}"
                ("user_id", "store_name", "key", "value")
                VALUES (:user_id, :store_name, :key, :value)
            ON CONFLICT ("user_id", "store_name", "key")
                DO UPDATE SET "value" = EXCLUDED."value"; ;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareRemoveStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            DELETE FROM "{tableName}" WHERE
                "user_id" = :user_id AND
                "store_name" = :store_name AND
                "key" = :key;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareClearStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            DELETE FROM "{tableName}" WHERE
                "user_id" = :user_id AND
                "store_name" = :store_name;
        ', $this->getTableName()));
    }
}