<?php

namespace Monogatari\RemoteStorage\StorageEngine;

use JsonException;
use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;
use Monogatari\RemoteStorage\Exception\StorageUnreadable;
use Monogatari\RemoteStorage\Exception\StorageUnwritable;

/**
 * FileSystemStorage stores data in file.
 */
class FileSystemStorage implements StorageInterface
{
    /**
     * Path to store the data.
     *
     * @var string
     */
    private $path;

    /**
     * Data object to save / load.
     *
     * @var object
     */
    protected $data;

    /**
     * Constructor
     *
     * @param string $path  The path of the data stored.
     */
    public function __construct(string $path)
    {
        $this->path = $path;
        $this->data = $this->getAllFromFile();
    }

    /**
     * {@inheritDoc}
     */
    function getAll(): object
    {
        return $this->data;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \JsonException If the content stored cannot be parsed as json.
     */
    public function get(string $key)
    {
        $data = $this->getAll();
        if (empty($data->{$key})) {
            throw new StorageKeyNotFound($key);
        }
        return $data->{$key};
    }

    /**
     * {@inheritDoc}
     */
    public function set(string $key, $value)
    {
        $this->data->{$key} = $value;
        $this->save();
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function remove(string $key)
    {
        unset($this->data->{$key});
        $this->save();
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function clear(): bool
    {
        return unlink($this->path);
    }

    /**
     * Save the content into the underlying storage file.
     *
     * @return boolean  If the storage update is successfully saved.
     *
     * @throws StorageUnwritable
     */
    private function save(): bool
    {
        $dir = dirname($this->path);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (!is_writable($dir)) {
            throw new StorageUnwritable($this->path, 'unable to write to the parent directory');
        }
        if (file_exists($this->path) && !is_writable($this->path)) {
            throw new StorageUnwritable($this->path, 'unable to write to the file');
        }

        // encode data to json string and store to the file.
        try {
            $json = json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        } catch (JsonException $e) {
            throw new StorageUnwritable($this->path, 'error encoding to json: ' . $e->getMessage());
        }

        // store data to file.
        if (file_put_contents($this->path, $json) === false) {
            throw new StorageUnwritable($this->path, 'unable to write content to the file');
        }
        return true;
    }

    /**
     * @throws \JsonException If the content stored cannot be parsed as json.
     */
    private function getAllFromFile(): object
    {
        // If file not found.
        if (!is_file($this->path)) {
            return new \stdClass();
        }

        // If the file is empty.
        $contents = file_get_contents($this->path);
        if (empty($contents)) {
            throw new StorageUnreadable(
                $this->path,
                'json file is empty'
            );
        }

        try {
            $data = json_decode($contents, false, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw new StorageUnreadable(
                $this->path,
                'json decode failed: ' . $e->getMessage()
            );
        }

        if (!is_object($data)) {
            // Corrupted or otherwise misformated.
            throw new StorageUnreadable(
                $this->path,
                'data stored is not an object'
            );
        }

        return $data;
    }
}