<?php

namespace Monogatari\RemoteStorage\StorageEngine;

use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;
use Monogatari\RemoteStorage\Exception\StorageUnwritable;

/**
 * StorageInterface stores key-value pairs.
 */
interface StorageInterface
{
    /**
     * Get the entire storage object.
     *
     * @return object
     */
    public function getAll(): object;

    /**
     * Get the value of a certain key
     *
     * @param string $key  The key of the attribute of the storage object.
     *
     * @return mixed  The value of the key.
     *
     * @throws StorageKeyNotFound
     */
    public function get(string $key);

    /**
     * Set the key in the storage to the given value.
     * Need to run save() to confirm.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return self
     *
     * @throws StorageUnwritable
     */
    public function set(string $key, $value);

    /**
     * Remove a key from the storage.
     * Need to run save() to confirm.
     *
     * @return self
     *
     * @throws StorageUnwritable
     */
    public function remove(string $key);

    /**
     * Clear the underly storage facility.
     *
     * @return boolean  If the storage is successfully cleared.
     */
    public function clear(): bool;
}
