<?php

namespace Monogatari\RemoteStorage\Exception;

/**
 * Represent situation where a storage was corrupted
 * or otherwise unwritable.
 */
class StorageUnwritable extends \Exception
{
    /**
     * Any identifier of the relevant storage.
     * @var string
     */
    private $id;

    /**
     * Further explain how the storage was corrupted.
     * @var string
     */
    private $reason;

    /**
     * Constructor.
     *
     * @param string $id      Any identifier of the relevant storage.
     * @param string $reason  Further explain how the storage was corrupted.
     */
    public function __construct(string $id, string $reason = '')
    {
        $this->id = $id;
        $this->reason = $reason;
        parent::__construct(empty($reason)
            ? sprintf('storage (%s) unwritable', $id)
            : sprintf('storage (%s) unwritable: %s', $id, $reason));
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getReason(): string
    {
        return $this->reason;
    }
}