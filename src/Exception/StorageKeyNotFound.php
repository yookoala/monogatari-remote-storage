<?php

namespace Monogatari\RemoteStorage\Exception;

/**
 * Throw this if a key is not found in a StorageInterface.
 */
class StorageKeyNotFound extends \Exception
{
    /**
     * Key that is not found in the storage.
     *
     * @var string
     */
    private $key = '';

    public function __construct(string $key)
    {
        parent::__construct("key \"{$key}\" not found in the storage");
        $this->key = $key;
    }

    /**
     * Getting the key that is not found.
     *
     * @return string
     */
    public function key(): string
    {
        return $this->key;
    }
}
