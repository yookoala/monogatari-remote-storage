<?php

/**
 * @file
 *
 * Dynamic configurations with parameters
 * from PHP.
 *
 * @see https://codeception.com/docs/06-ModulesAndHelpers#Dynamic-Configuration-With-Parameters
 * @see ../codeception.yml
 * @see acceptance.suite.yml
 */

return [
    'TEST_SERVER_PORT' => getenv('TEST_SERVER_PORT', true) ?: 8080,
];