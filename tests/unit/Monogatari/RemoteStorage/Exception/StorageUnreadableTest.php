<?php
namespace Monogatari\RemoteStorage\Test\Monogatari\RemoteStorage\Exception;

use Monogatari\RemoteStorage\Exception\StorageUnreadable;

/**
 * @covers \Monogatari\RemoteStorage\Exception\StorageUnreadable
 */
class StorageUnreadableTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testSomeFeature()
    {
        $id = 'id-' . rand(1, 100);
        $reason = 'reason ' . rand(1, 100);

        $e = new StorageUnreadable($id);
        $this->assertStringContainsString($id, $e->getMessage());
        $this->assertEquals($id, $e->getId());
        $this->assertEquals('', $e->getReason());

        $e = new StorageUnreadable($id, $reason);
        $this->assertStringContainsString($id, $e->getMessage());
        $this->assertStringContainsString($reason, $e->getMessage());
        $this->assertEquals($id, $e->getId());
        $this->assertEquals($reason, $e->getReason());
    }
}