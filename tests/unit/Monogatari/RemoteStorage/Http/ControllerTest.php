<?php
namespace Monogatari\RemoteStorage\Test\Monogatari\RemoteStorage\Http;

use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;
use Monogatari\RemoteStorage\Http\Controller;
use Monogatari\RemoteStorage\Http\StorageRequest;
use Monogatari\RemoteStorage\StorageEngine\FileSystemStorage;
use Monogatari\RemoteStorage\StorageEngine\SimpleStorageFactory;
use Monogatari\RemoteStorage\StorageEngine\StorageFactoryInterface;
use Monogatari\RemoteStorage\StorageEngine\StorageInterface;

/**
 * @covers \Monogatari\RemoteStorage\Http\Controller
 */
class ControllerTest extends \Codeception\Test\Unit
{
    /**
     * Implementation of both StreamFactoryInterface and
     * ResponseFactoryInterface of PSR-7.
     *
     * @var HttpFactory
     */
    private $httpFactory;

    /**
     * Implementation of StorageFactoryInterface
     *
     * @var StorageFactoryInterface
     */
    private $storageFactory;

    /**
     * File path for the FileSystemStorage.
     *
     * @var string
     */
    private $filePath;

    protected function _before()
    {
        $this->httpFactory = new HttpFactory();
        $this->filePath = './ControllerTest.json';
        $this->storageFactory = new SimpleStorageFactory(
            fn() => (new FileSystemStorage($this->filePath)));
    }

    protected function _after()
    {
        if (is_file($this->filePath)) {
            unlink($this->filePath);
        }
    }

    /**
     * Test getting all data stored in the storage.
     *
     * @return void
     */
    public function testHandleGetAll()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            null,
            false,
            ''
        ));

        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data, $value);
    }

    /**
     * Test getting all data stored in the storage.
     *
     * @return void
     */
    public function testHandleGetAllKeys()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            null,
            true,
            ''
        ));

        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals(array_keys($raw_data), $value);
    }

    /**
     * Test getting all data stored in the storage.
     *
     * @return void
     */
    public function testHandleGet()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test getting gallery (an empty object)
        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            'gallery',
            false,
            ''
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data['gallery'], $value);

        // Test getting Save_2 (an object with content)
        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            'Save_2',
            false,
            ''
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data['Save_2'], $value);

        // Test getting non-existing key
        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            'not exists',
            false,
            ''
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals([], $value, 'Should return an empty object by default.');
    }

    /**
     * Test creating new key-value by POST / PUT method.
     *
     * @return void
     */
    public function testHandleCreateByPostAndPut()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        $raw_data_to_add = [
            [
                'name' => 'hello save 2',
                'key' => 'value in save 2',
            ],
        ];
        $raw_data_expected = $raw_data;
        $raw_data_expected['Save_3'] = $raw_data_to_add;

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test creating new 'Save_3' object
        $response = $controller->handleRequest(new StorageRequest(
            'POST',
            null,
            'Save_3',
            false,
            json_encode($raw_data_to_add)
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_to_add, $value, 'Should return the same value as submitted.');
        $value = json_decode(file_get_contents($this->filePath), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_expected, $value, 'The underlying data should have the new key-value appended.');

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        // Test creating new 'Save_3' object
        $response = $controller->handleRequest(new StorageRequest(
            'PUT',
            null,
            'Save_3',
            false,
            json_encode($raw_data_to_add)
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_to_add, $value, 'Should return the same value as submitted.');
        $value = json_decode(file_get_contents($this->filePath), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_expected, $value, 'The underlying data should have the new key-value appended.');
    }

    /**
     * Test updating existing key-value by POST / PUT method.
     *
     * @return void
     */
    public function testHandleUpdateByPostAmdPut()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        $raw_data_to_update = [
            [
                'name' => 'hello new save 2',
                'key' => 'value in save 2',
            ],
        ];
        $raw_data_expected = $raw_data;
        $raw_data_expected['Save_2'] = $raw_data_to_update;

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test updating 'Save_2' object
        $response = $controller->handleRequest(new StorageRequest(
            'POST',
            null,
            'Save_2',
            false,
            json_encode($raw_data_to_update)
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_to_update, $value, 'Should return the same value as submitted.');
        $value = json_decode(file_get_contents($this->filePath), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_expected, $value, 'The underlying data should have the new key-value appended.');

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        // Test updating 'Save_2' object
        $response = $controller->handleRequest(new StorageRequest(
            'PUT',
            null,
            'Save_2',
            false,
            json_encode($raw_data_to_update)
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_to_update, $value, 'Should return the same value as submitted.');
        $value = json_decode(file_get_contents($this->filePath), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_expected, $value, 'The underlying data should have the new key-value appended.');
    }

    /**
     * Test updating with POST / PUT method without specifying key.
     *
     * @return void
     */
    public function testHandleUpdateWithoutKey()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test updating null key with POST
        $response = $controller->handleRequest(new StorageRequest(
            'POST',
            null,
            null,
            false,
            '"foobar"'
        ));
        $this->assertEquals(400, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($value['code'], 400);
        $this->assertEquals($value['error'], 'bad request');
        $this->assertEquals($value['error_description'], 'must specify key for create / update operation');

        // Test updating empty key with POST
        $response = $controller->handleRequest(new StorageRequest(
            'POST',
            null,
            '',
            false,
            '"foobar"'
        ));
        $this->assertEquals(400, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($value['code'], 400);
        $this->assertEquals($value['error'], 'bad request');
        $this->assertEquals($value['error_description'], 'must specify key for create / update operation');

        // Test updating null key object with PUT
        $response = $controller->handleRequest(new StorageRequest(
            'PUT',
            null,
            null,
            false,
            '"foobar"'
        ));
        $this->assertEquals(400, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($value['code'], 400);
        $this->assertEquals($value['error'], 'bad request');
        $this->assertEquals($value['error_description'], 'must specify key for create / update operation');

        // Test updating empty key object with PUT
        $response = $controller->handleRequest(new StorageRequest(
            'PUT',
            null,
            '',
            false,
            '"foobar"'
        ));
        $this->assertEquals(400, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($value['code'], 400);
        $this->assertEquals($value['error'], 'bad request');
        $this->assertEquals($value['error_description'], 'must specify key for create / update operation');

    }

    /**
     * Test deleting existing key-value by DELETE method.
     *
     * @return void
     */
    public function testHandleDelete()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        $raw_data_expected = $raw_data;
        unset($raw_data_expected['Save_2']);

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test removing 'Save_2' object
        $response = $controller->handleRequest(new StorageRequest(
            'DELETE',
            null,
            'Save_2',
            false,
            ''
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        //$this->assertEquals($raw_data['Save_2'], $value, 'Should return the same value as submitted.');
        $value = json_decode(file_get_contents($this->filePath), true, 512, JSON_THROW_ON_ERROR);
        $this->assertEquals($raw_data_expected, $value, 'The underlying data should have the specified key-value removed.');
   }

    /**
     * Test deleting entire storage by DELETE method.
     *
     * @return void
     */
    public function testHandleDeleteAll()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        $raw_data_expected = $raw_data;
        unset($raw_data_expected['Save_2']);

        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test removing 'Save_2' object
        $response = $controller->handleRequest(new StorageRequest(
            'DELETE',
            null,
            null,
            false,
            ''
        ));
        $this->assertEquals(200, $response->getStatusCode());
        $value = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        //$this->assertEquals($raw_data, $value, 'Should return the same value as submitted.');
        $this->assertFileDoesNotExist($this->filePath);
    }

    public function testMethodNotAllowed()
    {
        $raw_data = [
            'gallery' => [],
            'Save_2' => [
                'name' => 'hello',
                'key' => 'value',
            ],
        ];
        // reset underlying file content
        file_put_contents(
            $this->filePath,
            json_encode($raw_data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR)
        );

        $controller = new Controller(
            $this->storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        // Test removing 'Save_2' object
        $response = $controller->handleRequest(new StorageRequest(
            'STRANGE METHOD',
            null,
            null,
            false,
            ''
        ));
        $this->assertEquals(405, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('method not allowed', $response_data['error']);
    }

   public function testInternalError()
   {
       $storageFactory = new SimpleStorageFactory(function (StorageRequest $r) {
            return new class implements StorageInterface
            {
                public function getAll(): object {
                    throw new \Exception('exception from getAll()');
                }
                public function get(string $key) {
                    throw new \Exception('exception from get(' . $key . ')');
                }
                public function set(string $key, $value) {
                    throw new \Exception('exception from set(' . $key . ', ' . var_export($value, true) . ')');
                }
                public function remove(string $key) {
                    throw new \Exception('exception from remove(' . $key . ')');
                }
                public function clear(): bool {
                    throw new \Exception('exception from clear()');
                }
                public function save(): bool {
                    throw new \Exception('exception from save()');
                }
            };
        });

        $controller = new Controller(
            $storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            null,
            false,
            ''
        ));
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('internal error', $response_data['error']);
        $this->assertStringStartsWith('unable to read storage: ', $response_data['error_description']);

        $response = $controller->handleRequest(new StorageRequest(
            'GET',
            null,
            'someKey',
            false,
            ''
        ));
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('internal error', $response_data['error']);
        $this->assertStringStartsWith('unable to read "someKey" from storage: ', $response_data['error_description']);

        $response = $controller->handleRequest(new StorageRequest(
            'POST',
            null,
            'someKey',
            false,
            '"some value"'
        ));
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('internal error', $response_data['error']);
        $this->assertStringStartsWith('unable to save to internal storage', $response_data['error_description']);

        $response = $controller->handleRequest(new StorageRequest(
            'DELETE',
            null,
            null,
            false,
            ''
        ));
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('internal error', $response_data['error']);
        $this->assertEquals('unable to clear the storage', $response_data['error_description']);

        $response = $controller->handleRequest(new StorageRequest(
            'DELETE',
            null,
            'someKey',
            false,
            ''
        ));
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('internal error', $response_data['error']);
        $this->assertEquals('unable to read from storage', $response_data['error_description']);
    }

    public function testInternalRemoveNotFound()
    {
        $storageFactory = new SimpleStorageFactory(function (StorageRequest $r) {
            return new class implements StorageInterface
            {
                public function getAll(): object {
                    throw new \Exception('exception from getAll()');
                }
                public function get(string $key) {
                    throw new StorageKeyNotFound($key);
                }
                public function set(string $key, $value) {
                    throw new \Exception('exception from set(' . $key . ', ' . var_export($value, true) . ')');
                }
                public function remove(string $key) {
                    throw new \Exception('exception from remove(' . $key . ')');
                }
                public function clear(): bool {
                    throw new \Exception('exception from clear()');
                }
                public function save(): bool {
                    throw new \Exception('exception from save()');
                }
            };
        });

        $controller = new Controller(
            $storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        $response = $controller->handleRequest(new StorageRequest(
            'DELETE',
            null,
            'someKey',
            false,
            ''
        ));
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('not found', $response_data['error']);
    }

    public function testInternalRemoveError()
    {
        $storageFactory = new SimpleStorageFactory(function (StorageRequest $r) {
            return new class implements StorageInterface
            {
                public function getAll(): object {
                    throw new \Exception('exception from getAll()');
                }
                public function get(string $key) {
                    return ['some' => 'value'];
                }
                public function set(string $key, $value) {
                    throw new \Exception('exception from set(' . $key . ', ' . var_export($value, true) . ')');
                }
                public function remove(string $key) {
                    throw new \Exception('exception from remove(' . $key . ')');
                }
                public function clear(): bool {
                    throw new \Exception('exception from clear()');
                }
                public function save(): bool {
                    throw new \Exception('exception from save()');
                }
            };
        });

        $controller = new Controller(
            $storageFactory,
            $this->httpFactory,
            $this->httpFactory
        );

        $response = $controller->handleRequest(new StorageRequest(
            'DELETE',
            null,
            'someKey',
            false,
            ''
        ));
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertJson($response->getBody());
        $response_data = json_decode($response->getBody(), true);
        $this->assertEquals('error', $response_data['status']);
        $this->assertEquals('internal error', $response_data['error']);
    }

    public function testRequestFromEnvironment()
    {
        file_put_contents($this->filePath, 'hello');
        $request = Controller::requestFromEnvironment(
            ['PATH_INFO' => '/someStoreName/someKey', 'REQUEST_METHOD' => 'custom method'],
            [],
            $this->filePath
        );
        $this->assertEquals('CUSTOM METHOD', $request->method);
        $this->assertEquals('someStoreName', $request->store_name);
        $this->assertEquals('someKey', $request->key);
        $this->assertFalse($request->keys_listing_mode);
        $this->assertEquals('hello', $request->body);

        $request = Controller::requestFromEnvironment(
            ['PATH_INFO' => '/someKey', 'REQUEST_METHOD' => 'custom method'],
            [],
            $this->filePath
        );
        $this->assertEquals(null, $request->store_name);
        $this->assertEquals('someKey', $request->key);

        $exception_thrown = false;
        try {
         $request = Controller::requestFromEnvironment(
             ['PATH_INFO' => 'stringWithNoPrefixSlash', 'REQUEST_METHOD' => 'custom method'],
             [],
             $this->filePath
         );
        } catch (\Exception $e) {
            $exception_thrown = true;
        }
        $this->assertTrue($exception_thrown);
        $this->assertStringStartsNotWith($e->getMessage(), 'Unable to parse pathinfo');
    }
}