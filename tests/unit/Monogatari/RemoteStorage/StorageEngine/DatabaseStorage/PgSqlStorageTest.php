<?php
namespace Monogatari\RemoteStorage\Test\Monogatari\RemoteStorage\StorageEngine\DatabaseStorage;

use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;
use Monogatari\RemoteStorage\StorageEngine\DatabaseStorage\PgSqlStorage;

class PgSqlStorageTest extends \Codeception\Test\Unit
{
    /**
     * @var \Monogatari\RemoteStorage\Test\UnitTester
     */
    protected $tester;

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var string
     */
    private $tableName;

    protected function _before()
    {
        $dbHost = getenv('POSTGRES_HOSTNAME') ?: '127.0.0.1';
        $dbPort = getenv('POSTGRES_HOST_PORT') ?: '5432';
        $dbName = getenv('POSTGRES_DB') ?: 'test';
        $dbUser = getenv('POSTGRES_USER') ?: 'test';
        $dbPass = getenv('POSTGRES_PASSWORD') ?: 'password';
        $this->pdo = new \PDO(
            "pgsql:host={$dbHost};port={$dbPort};dbname={$dbName};sslmode=allow",
            $dbUser,
            $dbPass,
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            ],
        );
        $this->tableName = getenv('POSTGRES_TABLENAME') ?: 'monogatari_storage_table';

        // Remove the table
        $this->pdo->exec("DROP TABLE IF EXISTS \"{$this->tableName()}\";");
    }

    protected function _after()
    {
    }

    // tests constructor for creating base table
    public function testConstrustor()
    {
        // Initiate the table with constructor
        new PgSqlStorage($this->pdo, $this->tableName(), 'random_store', 'some_user_id');
        $this->assertNotFalse($this->pdo->query("SELECT * FROM \"{$this->tableName()}\""), 'Table should exists for query.');
    }

    // tests get and set an integer
    public function testSetAndGetInteger()
    {
        // Create and store a random value to test with.
        $key = 'rand_key_' . rand(0, 100);
        $value = rand(0, 100);
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        $storage->set($key, $value);

        $this->assertSame($value, $storage->get($key), 'Value retrieved should be of the same type and value before storing.');
    }

    // tests get and set an string
    public function testSetAndGetString()
    {
        // Create and store a random value to test with.
        $key = 'rand_key_' . rand(0, 100);
        $value = 'random_string_value_' . rand(0, 100);
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        $storage->set($key, $value);

        $this->assertSame($value, $storage->get($key), 'Value retrieved should be of the same type and value before storing.');
    }

    // tests get and set an array
    public function testSetAndGetArray()
    {
        // Create and store a random value to test with.
        $key = 'rand_key_' . rand(0, 100);
        $value = [
            'random_string_value_' . rand(0, 100),
            'random_string_value_' . rand(0, 100),
            'random_string_value_' . rand(0, 100),
            'random_string_value_' . rand(0, 100),
        ];
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        $storage->set($key, $value);

        $this->assertSame($value, $storage->get($key), 'Value retrieved should be of the same type and value before storing.');
    }

    // tests get and set an object
    public function testSetAndGetObject()
    {
        // Create and store a random value to test with.
        $key = 'rand_key_' . rand(0, 100);
        $value = (object) [
            'a' => 'random_string_value_' . rand(0, 100),
            'b' => 'random_string_value_' . rand(0, 100),
            'c' => 'random_string_value_' . rand(0, 100),
            'd' => 'random_string_value_' . rand(0, 100),
        ];
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        $storage->set($key, $value);

        $this->assertTrue(is_object($storage->get($key)), 'Value retrieved should be an object');
        $this->assertEquals($value, $storage->get($key), 'Value retrieved should be of the same type and value before storing.');
    }

    // tests get and remove vaolue
    public function testSetAndRemove()
    {
        // Create and store a random value to test with.
        $key = 'rand_key_' . rand(0, 100);
        $value = rand(0, 100);
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        $storage->set($key, $value);
        $this->assertSame($value, $storage->get($key), 'Value retrieved should be of the same type and value before storing.');

        $storage->remove($key);
        $e = null;
        try {
            $storage->get($key);
        } catch (StorageKeyNotFound $e) {
            // Do nothing.
        }
        $this->assertInstanceOf(StorageKeyNotFound::class, $e, 'Expect error to be StorageKeyNotFound');
        $this->assertSame($key, $e->key());
    }

    // tests get and remove vaolue
    public function testSetAndGetAll()
    {
        // Create and store a random value to test with.
        $values = [
            'rand_key_' . rand(0, 100) => rand(0, 100),
            'rand_key_' . rand(0, 100) => rand(0, 100),
            'rand_key_' . rand(0, 100) => rand(0, 100),
            'rand_key_' . rand(0, 100) => rand(0, 100),
        ];
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        foreach ($values as $key => $value) {
            $storage->set($key, $value);
        }

        $this->assertEquals((object) $values, $storage->getAll(), 'Get all gets an object of all the key-values previously stored.');
    }

    // tests get and remove vaolue
    public function testSetAndClear()
    {
        // Create and store a random value to test with.
        $values = [
            'rand_key_' . rand(0, 100) => rand(0, 100),
            'rand_key_' . rand(0, 100) => rand(0, 100),
            'rand_key_' . rand(0, 100) => rand(0, 100),
            'rand_key_' . rand(0, 100) => rand(0, 100),
        ];
        $storeName = 'rand_store_' . rand(0, 100);
        $storage = new PgSqlStorage($this->pdo, $this->tableName(), $storeName, 'some_user_id');
        foreach ($values as $key => $value) {
            $storage->set($key, $value);
        }
        $storage->clear();

        $this->assertIsObject($storage->getAll(), 'Get all gets an object');
        $this->assertEquals((object) [], $storage->getAll(), 'Get all gets an empty object.');
    }

    protected function pdo(): \PDO
    {
        return $this->pdo;
    }

    protected function tableName(): string
    {
        return $this->tableName;
    }
}