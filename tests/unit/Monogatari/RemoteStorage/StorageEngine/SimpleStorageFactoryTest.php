<?php
namespace Monogatari\RemoteStorage\Test\Monogatari\RemoteStorage\StorageEngine;

use Monogatari\RemoteStorage\Http\StorageRequest;
use Monogatari\RemoteStorage\StorageEngine\FileSystemStorage;
use Monogatari\RemoteStorage\StorageEngine\SimpleStorageFactory;

/**
 * @covers \Monogatari\RemoteStorage\StorageEngine\SimpleStorageFactory
 */
class SimpleStorageFactoryTest extends \Codeception\Test\Unit
{
    private $filePath = './SimpleStorageFactory.json';

    protected function _before()
    {
    }

    protected function _after()
    {
        if (is_file($this->filePath)) {
            unlink($this->filePath);
        }
    }

    public function testConstructor()
    {
        $expected_value = (object) ['hello' => 'world' . rand(1, 10)];
        file_put_contents($this->filePath, json_encode($expected_value));

        // This factory always return the same storage implementation.
        $factory = new SimpleStorageFactory(fn(StorageRequest $r) => new FileSystemStorage($this->filePath));
        $storage = $factory->makeStorage(new StorageRequest(
            // none of the request attribute matters
            '',
            null,
            null,
            false,
            ''
        ));

        $this->assertEquals($expected_value, $storage->getAll());
    }
}