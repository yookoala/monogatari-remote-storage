<?php

namespace Monogatari\RemoteStorage\Test;

use Monogatari\RemoteStorage\Test\AcceptanceTester;

class BasicExampleCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToSave(AcceptanceTester $I)
    {
        // On the demo game's front page.
        $I->am('Test Player');
        $I->wantTo('Save the game and see it in game save listing');
        $I->amOnPage('/basic/');
        $I->wait(1);

        // Start the game.
        $I->makeScreenshot();
        $I->amGoingTo('Start the game');
        $I->see('Start', ['css' => 'main-menu button']);
        $I->click('Start');
        $I->wait(1);

        // Got the name modification popup, fill in to proceed.
        $I->makeScreenshot();
        $I->amGoingTo('Fill in my name');
        $I->see('What is your name?');
        $I->see('OK', ['css' => 'button']);
        $I->fillField(['name' => 'field'], 'Test Player');
        $I->makeScreenshot();
        $I->click('OK');
        $I->wait(1);

        // Goto the save interface.
        $I->makeScreenshot();
        $I->amGoingTo('Open the game save interface');
        $I->see('Hi Test Player'); // See the greeting message.
        $I->see('Save', ['css' => 'quick-menu button']); // See the Save button on quick menu.
        $I->click('Save', ['css' => 'quick-menu']); // Click the Save button.
        $I->wait(1);

        // On game save interface, create a new save.
        $I->makeScreenshot();
        $I->amGoingTo('Save game');
        //$I->see('No saved games'); // Default message on the interface.
        $I->see('Save', ['css' => 'save-screen button']); // See the save button.
        $I->fillField(['css' => 'input[type=text][data-content=slot-name]'], 'Testing Save');
        $I->click('Save', ['css' => 'save-screen']); // Click the Save button.
        $I->click('save-screen button[data-action=back]'); // Click the back button.
        $I->wait(1);

        // Go back to save screen after save
        $I->makeScreenshot();
        $I->amGoingTo('Open the game save interface again');
        $I->see('Save', ['css' => 'quick-menu button']); // See the Save button on quick menu.
        $I->click('Save', ['css' => 'quick-menu']); // Click the Save button.
        $I->wait(1);

        // Check if the new save was created
        $I->makeScreenshot();
        $I->amGoingTo('See the saved game');
        $I->see('Testing save', ['css' => 'slot-container save-slot']);
    }
}
