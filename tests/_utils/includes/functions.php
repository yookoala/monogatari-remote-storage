<?php

function get_all_links_from(string $url): Iterable {
    $document = new \DOMDocument();
    $body = file_get_contents($url);
    @$document->loadHTML($body);
    $xpath = new DOMXpath($document);
    $list = $xpath->evaluate('//a[@href]');
    if (!$list instanceof \DOMNodeList) {
        throw new \Exception('unable to evaluate xpath as DOMNodeList');
    }
    foreach ($list as $element) {
        if (
            $element instanceof \DOMElement
            && $element->hasAttribute('href')
        ) {
            yield $element->textContent => $element->getAttribute('href');
        }
    }
}

function filter_links(
    Iterable $links,
    string $regex = '/^Link text (?<key>[\d\.]+)$/i',
): array {
    $downloads = [];
    foreach ($links as $name => $href) {
        if (preg_match($regex,  $name, $matches)) {
            $downloads[$matches['key']] = $href;
        }
    }
    return $downloads;
}

function file_get_xml_document(string $url): \DOMDocument {
    // Get the XML for the file index API.
    $xmlDom = new \DOMDocument();
    $xmlContents = file_get_contents($url);
    if (!$xmlDom->loadXML($xmlContents)) {
        throw new \Exception('Unable to parse the chromedriver file index API response as XML.');
    }
    return $xmlDom;
}

function get_nodes(
    \DOMDocument $document,
    string $xpathQuery,
    string $namespacePrefix,
    string $namespaceDocUrl,
): iterable {
    $xpath = new \DOMXPath($document);
    $xpath->registerNamespace($namespacePrefix, $namespaceDocUrl);
    $list = $xpath->query($xpathQuery, null, false);
    if (!$list instanceof \DOMNodeList) {
        throw new \Exception('Failed to evaulate the xpath into node list.');
    }

    foreach ($list as $node) {
        /** @var \DOMNode $node */
        yield $node->textContent;
    }
}
