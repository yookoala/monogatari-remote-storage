<?php

require_once __DIR__ . '/includes/SemVer.php';
require_once __DIR__ . '/includes/functions.php';

/**
 * Download the latest Chrome driver.
 *
 * @param string $targetDir   Target directory to extract the binary to.
 * @param string $filename    The expected filename of the driver in the downloaded archive file.
 *
 * @return string  Version string of the downloaded version.
 *
 * @throws \Exception
 */
function downloadChromeDriver(string $targetDir, string $filename): string {
    $targetPath = $targetDir . '/' . $filename;

    // Get the download link of the latest version of ChromeDriver.
    $downloadPages = filter_links(get_all_links_from('https://chromedriver.chromium.org/downloads'), '/^ChromeDriver (?<key>[\d\.]+)$/i');
    $versions = SemVer::parseSorted(array_keys($downloadPages));
    if (empty($versions)) throw new \Exception('Unable to find any chromedriver version.');
    $latestVersion = (string) array_pop($versions);

    // Read the AWS bucket XML from Google APIs server for the version.
    $serverUrl = 'https://chromedriver.storage.googleapis.com/'; // requires trailing slash
    $fileEntries = get_nodes(
        file_get_xml_document($serverUrl . '?' . http_build_query([
            'delimiter' => '/',
            'prefix' => $latestVersion . '/',
        ])),
        '/aws:ListBucketResult/aws:Contents/aws:Key',
        'aws',
        'http://doc.s3.amazonaws.com/2006-03-01',
    );

    // Download the zip file of the version.
    $filepaths = array_filter(iterator_to_array($fileEntries), fn($filepath) => preg_match('/_linux64\.zip$/', $filepath));
    $filepath = array_pop($filepaths);
    $src = fopen($serverUrl . $filepath, 'r');
    $tempzipfile = tempnam(sys_get_temp_dir(), 'tmpzip_');
    $dest = fopen($tempzipfile, 'w');
    stream_copy_to_stream($src, $dest);
    fclose($src);
    fclose($dest);

    // Unzip the chromedriver binary to a file.
    $zip = new \ZipArchive();
    if ($zip->open($tempzipfile)) {
        file_put_contents($targetPath, $zip->getFromName($filename));
        chmod($targetPath, 0755);
    }

    // Clean up.
    unlink($tempzipfile);

    return $latestVersion;
}

/**
 * Download the latest Gecko driver.
 *
 * @param string $targetDir   Target directory to extract the binary to.
 * @param string $filename    The expected filename of the driver in the downloaded archive file.
 *
 * @return string  Version string of the downloaded version.
 *
 * @throws \Exception
 */
function downloadMonogatari(string $targetDir, string $filename): string {
    $context = stream_context_create([
        'http' => [
            'method' => 'GET',
            'header' => implode("\r\n", array_map(fn($item) => implode(': ', $item), [
                ['User-Agent', 'php'],
            ])),
            'ignore_errors' => true,
        ],
    ]);
    $fh = fopen('https://api.github.com/repos/mozilla/geckodriver/releases', 'r', false, $context);
    $contents = stream_get_contents($fh);
    $response = json_decode($contents, false, 512, JSON_THROW_ON_ERROR);
    if (!is_array($response)) {
        throw new \Exception("Contents from GitHub API is not a valid JSON array:\n" . $contents);
    }
    $downloadUrls = array_reduce(
        $response,
        function ($carry, $current) {
            if (!empty($current->assets)) {
                $asset_urls = array_filter(
                    array_map(fn($asset) => $asset->browser_download_url, $current->assets),
                    fn($url) => preg_match('/geckodriver\-(?<version>[v\d\.]+)\-linux64\.tar\.gz$/', $url),
                );
                if (!empty($current->tag_name) && !empty($asset_urls)) {
                    $carry[$current->tag_name] = array_shift($asset_urls);
                }
            }
            return $carry;
        },
        [],
    );

    $versions = SemVer::parseSorted(array_keys($downloadUrls));
    if (empty($versions)) {
        throw new \Exception('Unable to parse the versions of geckodriver');
    }
    $latestVersion = (string) array_pop($versions);

    $src = fopen($downloadUrls["v{$latestVersion}"], 'r');
    $tempzipfile = sys_get_temp_dir() . '/tmp_' . substr(md5(random_bytes(100)), 5) . '.tar.gz';
    $dest = fopen($tempzipfile, 'w');
    stream_copy_to_stream($src, $dest);
    fclose($src);
    fclose($dest);

    // Unzip the chromedriver binary to a file.
    if (is_file($targetDir . '/' . $filename) && !unlink($targetDir . '/' . $filename)) {
        throw new \Exception('Unable to remove previously downloaded geckodriver file.');
    }
    $archive = new \PharData($tempzipfile);
    $archive->extractTo($targetDir);
    if (!is_file($targetDir . '/' . $filename)) {
        throw new \Exception('Unable to find the decompressed geckodriver.');
    }

    return $latestVersion;
}

// Force create folder for download webdrivers.
$outputDir = __DIR__ . '/_generated';
if (!is_dir($outputDir)) {
    mkdir($outputDir, 0755, true);
}

// Parse option flags.
$options = getopt("hf");
if (isset($options['h'])) {
    echo "Usage: " . basename(__FILE__) . " [-fh]\n";
    echo "\n";
    echo "  -h   Help message.\n";
    echo "  -f   Force download new files. Will download even if previous download exists.\n";
    exit(0);
}

// Downloads
if (isset($options['f']) || !is_file($outputDir . '/chromedriver')) {
    echo "Download chromedriver";
    $version = downloadChromeDriver($outputDir, 'chromedriver');
    echo " ... version: {$version}\n";
} else {
    echo "Already has a chromedriver. Skip download.\n";
}
if (isset($options['f']) || !is_file($outputDir . '/geckodriver')) {
    echo "Download geckodriver";
    $version = downloadMonogatari($outputDir, 'geckodriver');
    echo " ... version: {$version}\n";
} else {
    echo "Already has a geckodriver. Skip download.\n";
}