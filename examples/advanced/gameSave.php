<?php

/**
 * Advanced example of how the Monogatari RemoteStroage library works.
 *
 * Demonstrate how the key workflow steps. Can be easily converted to
 * service container / DI container architecture.
 *
 * Requires the "guzzlehttp/psr7" library. Run `composer install` to
 * install it through dev-dependencies before trying the example.
 *
 * Requires a copy of the Monogatari distribution source code to work.
 * Please run `composer test:download:monogatari` to download, then
 * serve this folder.
 *
 * You should also supply the mysql information with the MYSQL_*
 * environment variables here. Or you should modify how $db* variables
 * are defined.
 */

use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\ServerRequest;
use Monogatari\RemoteStorage\Http\Controller;
use Monogatari\RemoteStorage\Http\StorageRequest;
use Monogatari\RemoteStorage\StorageEngine\DatabaseStorage\MySqlStorage;
use Monogatari\RemoteStorage\StorageEngine\SimpleStorageFactory;

require_once __DIR__ . '/../../vendor/autoload.php';

// Connect to database
$dbHost = getenv('MYSQL_HOSTNAME') ?: '127.0.0.1';
$dbPort = getenv('MYSQL_PORT') ?: '3306';
$dbName = getenv('MYSQL_DATABASE') ?: 'test';
$dbUser = getenv('MYSQL_USER') ?: 'test';
$dbPass = getenv('MYSQL_PASSWORD') ?: 'password';
$pdo = new \PDO("mysql:host={$dbHost};port={$dbPort};dbname={$dbName}", $dbUser, $dbPass);

// Prepare Controller
$httpFactory = new HttpFactory();
$controller = new Controller(
    new SimpleStorageFactory(
        fn (StorageRequest $request) => new MySqlStorage(
            $pdo,
            'monogatari_storage',
            $request->store_name,
            $request->context['username'] ?? '',
        ),
    ),
    $httpFactory, // StreamFactoryInterface implementation
    $httpFactory, // ResponseFactoryInterface implementation
);

// Parse StoreRequest from PSR-7 ServerRequest
$request = ServerRequest::fromGlobals();
if (!preg_match('/\/gameSave\.php(|\/((?<store_name>.+)\/|)(?<key>.*?))$/', $request->getUri()->getPath(), $uriVariables)) {
    throw new \Exception('Unable to parse uri: ' . $request->getUri()->getPath());
}
$uriVariables += ['store_name' => null, 'key' => null];
preg_match('/^(?<username>.+?)(|:(?<password>.*))$/', $request->getUri()->getUserInfo() ?? '', $userContext);
$userContext += ['username' => '', 'password' => ''];
$storageRequest = new StorageRequest(
    $request->getMethod(),
    $uriVariables['store_name'],
    $uriVariables['key'],
    ($request->getQueryParams()['keys'] ?? null) === 'true',
    $request->getBody(),
    $userContext,
);

// Generate PSR-7 Response
$response = $controller->handleRequest($storageRequest);

// Emit the Response
Controller::emit($response);